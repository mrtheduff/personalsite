﻿using PersonalSite.UI.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace PersonalSite.UI.MVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Portfolio()
        {
            return View();
        }

        public ActionResult Resources()
        {
            return View();
        }

        //GET
        public ActionResult Contact()
        {
            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //Create the body for email message
            string body = string.Format(
                    "Name: {0}<br/>Email: {1}<br/>Subject: {2}" +
                    "<br/>Message:<br/>{3}",
                    contact.Name,
                    contact.Email,
                    contact.Subject,
                    contact.Message
                );

            //Debug.WriteLine(body); 

            //Create and configure the Mail Message 
            MailMessage msg = new MailMessage(
                "no-reply@jeffdcodefolio.com", //From address
                "jeff.demaranville@gmail.com", //To address
                contact.Subject, //Subject of message
                body //Body of message defined above
                );

            //Additional configs for Mail message obj
            msg.IsBodyHtml = true;


            //Create and configure the SMTP client
            SmtpClient client = new SmtpClient("mail.jeffdcodefolio.com");
            client.Credentials = new NetworkCredential("no-reply@jeffdcodefolio.com", "Pwnx0rd!!");

            if (ModelState.IsValid)
            {
                using (client)
                {

                    try
                    {
                        client.Send(msg);
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was an error sending your mail message. Please try again.";
                        return View();
                    }
                }
                return View("ContactConfirmation", contact);
            }
            return View();
        }

        public ActionResult Resume()
        {
            return View();
        }

        public ActionResult Blog()
        {
            return View();
        }
    }
}