﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PersonalSite.UI.MVC.Models
{

    public class ContactViewModel
    {
        [Required(ErrorMessage = "* Required")]
        [StringLength(50, ErrorMessage = "* 50 Characters Max")]
        public string Name { get; set; }

        [StringLength(50, ErrorMessage = "* 50 Characters Max")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(75, ErrorMessage = "* 75 Characters Max")]
        public string Email { get; set; }

        [Required(ErrorMessage = "* Required")]
        [StringLength(280, ErrorMessage = "* 280 Characters Max")]
        public string Message { get; set; }
    }

}