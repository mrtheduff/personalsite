﻿$(function () {
    


    //Back to top button
    $(window).on('scroll', function () {
        if ((($('body').scrollTop() > 20 || $(document.documentElement).scrollTop() > 20) && $(window).width() <= 768)) {
            console.log(window.innerWidth);
            console.log(window.innerHeight);
            console.log($('body').scrollTop());
            $('#backToTop').fadeIn(700);
        } else {
            $('#backToTop').fadeOut();
        }
    });


    //Hide nav on scroll
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('nav.navbar-fixed-top').outerHeight();

    $(window).scroll(function (event) {
        didScroll = true;
    });
    console.log(window);

    setInterval(function () {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();

        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if ($(window).width() <= 678 || $(window).width() > $(window).height()) {
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                if ($('.navbar-collapse').hasClass('collapse')) {
                    $('nav.navbar-fixed-top').removeClass('nav-down').addClass('nav-up');
                }
                else {
                    $('nav.navbar-fixed-top').removeClass('nav-down').addClass('nav-expanded-up');
                }
                

            }
            else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('nav.navbar-fixed-top').removeClass('nav-up');//.addClass('nav-down');
                    $('nav.navbar-fixed-top').removeClass('nav-expanded-up');
                }
            }

        }

        lastScrollTop = st;
    }
});